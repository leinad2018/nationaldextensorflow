from bs4 import BeautifulSoup
import requests

# import the necessary packages
from keras.preprocessing import image as image_utils
from keras.applications.imagenet_utils import decode_predictions
from keras.applications.imagenet_utils import preprocess_input
from keras.applications import VGG16
import numpy as np
import argparse


def classifyImage(path):
    # construct the argument parser and parse the arguments

    image = image_utils.load_img(path, target_size=(224, 224))
    image = image_utils.img_to_array(image)

    image = np.expand_dims(image, axis=0)
    image = preprocess_input(image)

    #image = np.expand_dims(image, axis=0)
    #image = preprocess_input(image)

    # print("[INFO] loading network...")
    model = VGG16(weights="imagenet")

    # classify the image
    print("[INFO] classifying image...")
    preds = model.predict(image)
    P = decode_predictions(preds)

    # loop over the predictions and display the rank-5 predictions +
    # probabilities to our terminal
    #for (i, (imagenetID, label, prob)) in enumerate(P[0]):
        #print("{}. {}: {:.2f}%".format(i + 1, label, prob * 100))
    return P[0]



text = ".\\cheetah.jpg"
top = classifyImage(text)
#print(top[0][1])
animal = top[0][1]
print(animal)

try:
    html_doc = requests.get("https://en.wikipedia.com/wiki/" + animal)
    html_data = html_doc.text
    soup = BeautifulSoup(html_data, 'html.parser')
    table = soup.find_all("table", class_="infobox")[0]
    print(table.getText())
except Exception:
    print("Invalid animal")
